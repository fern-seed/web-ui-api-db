# Fern Seed

a foundation to configure a full-stack web-app  
focused on devx with a clear path toward production
a minimalist and flexible container scaffolding  
a pattern for structuring web apps  
a repeatable way to organize projects  
a meta framework to use as an application architecture template  
a blank slate, a fresh start

[https://gitlab.com/fern-seed/web-ui-api-db](https://gitlab.com/fern-seed/web-ui-api-db)

https://opensource.guide/starting-a-project/

## Copy / Clone

```
npx degit gitlab:fern-seed/web-ui-api-db#main boilerplate
# or see below for git clone
```

Rename `ui/src/components/blerplate-card.vue` to use your project name prefix. (Filename intentionally misspelled to prevent being replaced.)

## Getting started

```
cd boilerplate/ui
npm install
npm run dev
```

### Run tests

For tests, set the BASE_URL in the shell first

```
export CYPRESS_BASE_URL=http://localhost:3000
export CYPRESS_API_URL=http://localhost:3000/api
npx cypress open
```

### Launch the API server

Open and customize the docker-compose.yml file

May not be necessary for UI development only, but gets docs running quickly.

See [README-docker.md](README-docker.md)

## Conventions / Patterns

All project based variables should be based on `boilerplate` stem.  
That way, once `boilerplate` is globally replaced, other instances can still be found and updated (as needed).

```
boilerplate
boilerplatenodash
boilerplate-api
boilerplate-url
boilerplate.local
boilerplate-title
boilerplate-docs-description
boilerplate-documentation-description
boilerplate-org
boilerplate-group
boilerplate-project
boilerplate-author
```

use of org and group is optional. If they're the same, replace the one you don't want to use with the one you want.

Choosing a new project stem

This term is important to use consistently.
dashes usually ok (not in `boilerplatenodash`), low lines usually ok, _no spaces_, ideally no capitalization

https://gitlab.com/boilerplate-group/boilerplate-project/-/issues
the project's issues URL

https://gitlab.com/boilerplate-group/boilerplate-project
the project's repository URL

TODO:admins@boilerplate-org.email
Point of contact:

boilerplate-author
Copyright and username references

Move from more specific to less specific to match all cases

use the string 'boilerplate-project' for printable name
anything goes here, including spaces and capitalization, as long as it's a permanent string. This string should not change over time. If it does change in one place it should be changed everywhere.

### Find / Replace

Find and replace all instances of `boilerplate` with `project_name`. Use VS Code to find and replace across all files and see what is being matched. Be careful if any of the strings appear in your .git directory.

git add .
git commit -m "update project name and references"

## Containers

This pattern leverages docker containers and docker-compose. If you're new to containers or need to get docker installed, [see here](https://gitlab.com/charlesbrandt/public/-/blob/main/system/virtualization/docker.md).

If your application requires an API, it will be worth understanding how to run the docker containers for development.

[Infrastructure configuration (Dev-Ops)](README-docker.md)

## Additional documentation

    data/\*
    ui/content/\*

## Using git repos as templates

[Having a git repo that is a template for new projects](https://medium.com/@smrgrace/having-a-git-repo-that-is-a-template-for-new-projects-148079b7f178) by Sam Grace is a well written overview of the process.

https://betterprogramming.pub/forget-boilerplate-use-repository-templates-74efebbee8eb

## Development

When creating a repository that needs to track upstream changes:

```
git clone -b main --single-branch git@gitlab.com:fern-seed/web-ui-api-db.git boilerplate
cd boilerplate
git remote -v
git remote rename origin upstream
git remote set-url --push upstream no_push
git remote -v
```

Set up the path to your own project's git server (e.g. gitlab, github or a local git server):

```
git remote add origin git@gitlab.com:fern-seed/boilerplate.git
git push -u origin main
# or just
git push -u origin
```

### Add to existing projects

Already have an existing project that would benefit from an improved structure? Add in web-ui-api-db to the upstream

```
git remote -v

git remote add upstream https://gitlab.com/fern-seed/web-ui-api-db

git fetch upstream
git merge --allow-unrelated-histories upstream/main
# or whichever branch you want to merge
```

### Pull in upstream updates

To track changes on a remote upstream web-ui-api-db and merge them in with your repo:

```
git remote -v

git remote add upstream https://gitlab.com/fern-seed/web-ui-api-db

git fetch upstream
git merge upstream/main
```

or just ?

```
git pull upstream main
```

### Handling files with same name

If you want to keep the original around as a reference, just `git mv oldname newname`.

If you want to keep things clean

    git rm README.md

This breaks the upstream association so you won't get conflicts in your different files with the same name when trying to merge upstream.
