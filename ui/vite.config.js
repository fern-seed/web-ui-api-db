import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from "path";
import Icons from "unplugin-icons/vite";
import IconsResolver from "unplugin-icons/resolver";
import Components from "unplugin-vue-components/vite";

export default defineConfig({
  plugins: [
    vue(),
    Components({
      resolvers: IconsResolver(),
    }),
    Icons({
      /* options */
    }),
  ],
  resolve: {
    alias: {
      "@": resolve(__dirname, "src"),
    },
  },
  server: {
    // https://vitejs.dev/config/#server-proxy
    // useful when running vite on localhost
    // as the primary web /dev server
    proxy: {
      "/api": {
        target: "http://localhost:3030",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },

    // only necessary when running vite behind an nginx proxy
    // https://vitejs.dev/config/#server-hmr
    // https://github.com/vitejs/vite/discussions/4795
    hmr: {
      // vite@2.5.1 and older
      port: 3000,
      // vite@2.5.2 and newer:
      // clientPort: 8888,
    },

    // https://vitejs.dev/config/#server-https
    // https: true,

    // automatically attempts to open a browser
    // fails in docker
    // open: true,
  },
});
