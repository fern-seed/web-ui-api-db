// start-here.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

describe("My First Test", () => {
  it("Does not do much!", () => {
    expect(true).to.equal(true);
  });
});

describe("My Second Test", () => {
  it("Goes to the home page", () => {
    // Note that cypress is running in docker,
    // so all urls need to use the container names
    // ideally set the CYPRESS_baseUrl value in `docker-compose.yml`
    cy.visit("");
    // cy.visit("http://boilerplate_ui:3000");

    // this would mean the webserver is running in the cypress container!
    //cy.visit("https://boilerplate.local");

    cy.viewport(1000, 1000);

    // at this point, check for some content on the page

    // open some links for reference on writing tests
  });
});
